const BN = web3.utils.BN;

const duration = {
  seconds: function (val) { return new BN(val); },
  minutes: function (val) { return new BN(val).mul(this.seconds('60')); },
  hours: function (val) { return new BN(val).mul(this.minutes('60')); },
  days: function (val) { return new BN(val).mul(this.hours('24')); },
  weeks: function (val) { return new BN(val).mul(this.days('7')); },
  years: function (val) { return new BN(val).mul(this.days('365')); },
};

const AwesomeToken = artifacts.require("AwesomeToken");
const AwesomeCrowdsale = artifacts.require("AwesomeCrowdsale");

module.exports = async function(deployer, network, accounts) {
    await deployer.deploy(AwesomeToken, "AwesomeToken", "AWESOME", 18);
    const awesomeToken = await AwesomeToken.deployed();
    console.log(awesomeToken.address);

    const rate = 1000;
    const wallet = accounts[0];
    const timeNow = Math.floor(Date.now() / 1000);
    const openingTime = timeNow + duration.seconds(30);
    const closingTime = timeNow + duration.years(1);
    const cap = web3.utils.toWei(new BN(100));

    await deployer.deploy(
        AwesomeCrowdsale,
        rate,
        wallet,
        awesomeToken.address,
        openingTime,
        closingTime,
        cap);

    const awesomeCrowdsale = await AwesomeCrowdsale.deployed();
    console.log(awesomeCrowdsale.address);
};
